const path=require("path");
const HtmlWebpackPlugin=require("html-webpack-plugin");
module.exports={
    entry:'./src/index.tsx',
    output:{
        path:path.resolve(__dirname,"build"),
        filename:"bundle.js"
    },
    module:{
        rules:[
            {
                test:/\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
        },
        {
            test: /\.css$/,
             use: ['style-loader', 'css-loader']
          } ,
        {
            enforce: "pre",
      test: /\.js$/,
      loader: "source-map-loader"

        }
    ],
   
    },
    plugins:[
        new HtmlWebpackPlugin({
            template:'./index.html'
        })
    ],
    devServer: {
        historyApiFallback: true,
      },
    devtool: "source-map",
    resolve: {
        extensions: [".js", ".ts", ".tsx",".css"]
      }
}