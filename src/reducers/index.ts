import { combineReducers } from "redux";
import visibilityFilter from "./visibilityFilter";
import todos from "./todos";

  
  //let createStoreWithMiddleware = applyMiddleware(logger)(createStore)
  
export default combineReducers({ todos, visibilityFilter });
