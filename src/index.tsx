import * as React from "react";
import * as ReactDOM from "react-dom";
import App from './components';
import {Users} from './components/users';
import {Contact} from './components/contact'
import { Provider } from 'react-redux';

import 'bootstrap/dist/css/bootstrap.min.css';
import { Route,Switch, Link, BrowserRouter as Router } from 'react-router-dom'
import { Header } from "./components/header";

import { createStore,applyMiddleware } from "redux";
import rootReducer from "./reducers";

const logger = (store:any) => (next:any) => (action:any)=> {
    console.group(action.type)
    console.info('dispatching', action)
    let result = next(action);
    console.log('next state', store.getState());
    console.groupEnd()
    return result
  }
  
let createStoreWithMiddleware = applyMiddleware(logger)(createStore)
  
export  const newStore:any =createStoreWithMiddleware(rootReducer);


const ROOT=document.querySelector(".container");
const routing = (
    <div>
    <Provider store={newStore}>
    <Header>
    </Header>
    <Router>
        <Route exact path="/" component={App} />
        <Route exact path="/users" component={Users} />
        <Route exact path="/contact" component={Contact} />
    
    </Router>
    </Provider>
    </div>


  )
ReactDOM.render(routing,ROOT);