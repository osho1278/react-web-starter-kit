import * as React from 'react'
import {Navbar,Form,Nav,FormControl,Button} from 'react-bootstrap';
export class Header extends React.Component<any, any> {
  render() {
    return (
        <div>
    <Navbar bg="dark" variant="dark">
      <Navbar.Brand href="#home">Navbar</Navbar.Brand>
      <Nav className="mr-auto">
        <Nav.Link href="/">Home</Nav.Link>
        <Nav.Link href="/users">Users</Nav.Link>
        <Nav.Link href="/contact">Contact</Nav.Link>
      </Nav>
      <Form inline>
        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
        <Button variant="outline-info">Search</Button>
      </Form>
    </Navbar>
    <br />
    
    </div>
    )
  }
    
}
// default Contact