import * as React from "react";
import { Button, Alert, ButtonToolbar } from "react-bootstrap";
import { connect } from "react-redux";
import { addTodo } from "../actions";
//import {View} from 'react';
interface Props {
  counter: any;
  addTodo:Function;
}
class App extends React.Component<Props, any> {
  props: Props;

  constructor(props: any) {
    super(props);
  }

  componentDidMount(){
    console.log('The componenet   has mounted');
    this.props.addTodo('hello world');
  }

  render() {
    return (
      <div>
        <ButtonToolbar>
          <Button variant="primary">Primary</Button>
          <Button variant="secondary">Secondary</Button>
          <Button variant="success">Success</Button>
          <Button variant="warning">Warning</Button>
          <Button variant="danger">Danger</Button>
          <Button variant="info">Info</Button>
          <Button variant="light">Light</Button>
          <Button variant="dark">Dark</Button>
          <Button variant="link">Link</Button>
        </ButtonToolbar>
        <Alert variant="success">This is a lol alert—check it out!</Alert>

        <Button>Hello World</Button>
        <div className="jumbotron">
          <h1 className="display-4">Amazing React, Bootstrap and Webpack</h1>
          <p className="lead">Created with love</p>
          <hr className="my-4" />
          <p>
            It uses utility classNamees for typography and spacing to space
            content out within the larger container.
          </p>
          <p className="lead">
            <a className="btn btn-primary btn-lg" href="#" role="button">
              Learn more
            </a>
          </p>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: any /*, ownProps*/) => {
  console.log(state);
  return {
    counter: state.counter
  };
};

const mapDispatchToProps = { addTodo };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
